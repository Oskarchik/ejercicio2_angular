import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { CatalogComponent } from './catalog/catalog.component'
import { DetailComponent } from './catalog/detail/detail.component'
import { FormsModule } from '@angular/forms'

@NgModule({
  declarations: [AppComponent, CatalogComponent, DetailComponent],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
