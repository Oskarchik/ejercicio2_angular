import { Component, OnInit } from '@angular/core'
import { Catalog, Image } from './models/catalog'

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss'],
})
export class CatalogComponent implements OnInit {
  public catalog: Catalog
  urlImgToSend: string
  detailImageUrl: string = ''
  obj: Image
  constructor() {
    this.catalog = {
      gallery: [
        {
          urlImg: 'assets/cliff.jpg',
          urlName: 'acantilado',
          description: 'Lorem ipsum dolor sit amet consectetur ',
        },
        {
          urlImg: 'assets/leaf.jpg',
          urlName: 'hoja',
          description: 'Lorem ipsum dolor sit ',
        },
        {
          urlImg: 'assets/sky.jpg',
          urlName: 'cielo',
          description: 'Lorem ipsum dolor sit amet ',
        },
        {
          urlImg: 'assets/sunset.jpg',
          urlName: 'puesta de sol',
          description: 'Lorem ipsum dolor sit amet consectetur adipisicing',
        },
        {
          urlImg: 'assets/wheat.jpg',
          urlName: 'trigo',
          description: 'Lorem ipsum dolor sit amet consectetur ',
        },
      ],
    }
  }

  ngOnInit(): void {}
  sendPic(url: string) {
    this.urlImgToSend = url
    console.log(this.urlImgToSend)
  }
  setImage(urlImage: string, urlName: string, description: string): void {
    this.obj = {
      urlImg: urlImage,
      urlName: urlName,
      description: description,
    }
    this.catalog.gallery.push(this.obj)
  }
}
