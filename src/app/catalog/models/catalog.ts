export interface Catalog {
  gallery: Array<Image>
}
export interface Image {
  urlImg: string
  urlName: string
  description: string
}
