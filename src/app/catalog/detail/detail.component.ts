import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core'
import { Image } from '../models/catalog'

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  @Input() urlImgToSend: string
  @Output() emitPicture = new EventEmitter<string>()
  urlImage: string = ''

  constructor() {}

  ngOnInit(): void {}
  sendPicture() {
    this.emitPicture.emit(this.urlImage)
  }
}
